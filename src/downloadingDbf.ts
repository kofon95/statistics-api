/* eslint-disable max-classes-per-file */
import fs from 'fs';
import path from 'path';
import Nightmare from 'nightmare';
import TwoCaptchaClient from '@infosimples/node_two_captcha';

require('nightmare-inline-download')(Nightmare);

declare class DownloadedFileInfo {
  filename: string

  mimetype: string

  receivedBytes: number

  totalBytes: number

  url: string

  path: string

  state: string
}

const PAGE_RELOAD_MLS = 3_000;
export const ENCODING = 'cp866';
const STATISTICS_PAGE = 'http://stat.customs.ru/apex/f?p=201:3:3122166337457930::NO';

type TnvedOptions = '2' | '4' | '6' | '8' | '10';

declare class DataProviderOptions {
  twoCaptchaKey: string

  downloadDir: string
}

export class CustomsStatisticsProvider {
  downloadDir: string

  twoCaptchaClient: TwoCaptchaClient

  nightmare: Nightmare

  constructor(options: DataProviderOptions) {
    this.downloadDir = options.downloadDir;
    this.twoCaptchaClient = new TwoCaptchaClient(options.twoCaptchaKey, {
      timeout: 600_000,
      polling: 5_000,
      throwErrors: false,
    });
    this.nightmare = new Nightmare({
      show: true,
      // To avoid cross-device link issues with rename
      paths: {
        downloads: path.resolve(this.downloadDir),
      },
    });
  }

  private async downloadDbfFileWithoutTimeout(yearAndMonth: [number, number],
    tnvedDigits: TnvedOptions) : Promise<DownloadedFileInfo> {
    const year = yearAndMonth[0].toString();
    const month = yearAndMonth[1].toString().padStart(2, '0');

    /* eslint-disable no-undef */
    /* eslint-disable no-shadow */
    console.log(`Visiting: ${STATISTICS_PAGE}`);
    const captchaInBase64 = await this.nightmare
      .goto(STATISTICS_PAGE)
      .wait('body')
      .evaluate(((tnvedDigits: TnvedOptions) => {
        (document.getElementById('P3_TNVED') as HTMLInputElement).value = tnvedDigits;
        document.getElementById('P3_CH_PER_3').click();
      }), tnvedDigits)
      .wait(PAGE_RELOAD_MLS)
      .evaluate(((date: string) => { // e.g. "201701"
        (document.getElementById('P3_MONTH') as HTMLInputElement).value = date;
      }), year + month)
      // Enable/disable downloading info about all the federal subjects
      // .evaluate(() => {
      //   document.getElementById('P3_FEDERAL_SUBJECT_2').click();
      // })
      // .wait(PAGE_RELOAD_MLS)
      // .evaluate((c) => {
      //   (document.getElementById('P3_FEDERAL_SUBJECT2_2') as HTMLInputElement).value = c;
      // }, 82000)
      // ----------------------------
      .evaluate(() => {
        const img = document.getElementById('image') as HTMLImageElement;
        const canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        canvas.getContext('2d').drawImage(img, 0, 0);
        return canvas.toDataURL('image/jpeg');
      }) as string; // canvas.toDataURL returns string, but Nightmare denotes T :(

    console.log('Got the captcha, trying to guess...');
    const { text: captchaText } = await this.twoCaptchaClient.decode({ base64: captchaInBase64 });
    console.log(`Captcha was guessed as "${captchaText}"`);

    const dbfFile = await this.nightmare
      .evaluate((captchaText: string) => {
        (document.getElementById('P3_CAPTCHA_USER_TEXT') as HTMLInputElement).value = captchaText;
        // for some reason, in 1/10 cases, submitting does not work, but check() always does
        // (window as any).check();
      }, captchaText)
      .click('#B279457093780833677')
      .wait(PAGE_RELOAD_MLS)
      .evaluate(() => (document.getElementById('P3_CAPTCHA_USER_TEXT') as HTMLInputElement).value !== 'Неправильно введен код')
      /* eslint-enable no-shadow */
      /* eslint-enable no-undef */
      .then((success: boolean) => {
        if (!success) {
          const filename = path.resolve(this.downloadDir, `captcha_${captchaText}.base64`);
          console.log(`Wrong captcha was downloaded to ${filename}`);
          fs.writeFile(filename, captchaInBase64, {}, () => {});
          throw new Error('Wrong captcha');
        }
        const filename = `${tnvedDigits}_${year}-${month}.dbf`;
        console.log('Ok. Start downloading:', filename);
        return this.nightmare.download(path.resolve(this.downloadDir, filename));
      });
    return dbfFile;
  }

  async downloadDbfFile(
    yearAndMonth: [number, number],
    tnvedDigits: TnvedOptions,
    timeoutMls: number,
  ) : Promise<DownloadedFileInfo> {
    const nightmareHalter = setTimeout(this.nightmare.halt, timeoutMls);

    try {
      return this.downloadDbfFileWithoutTimeout(yearAndMonth, tnvedDigits);
    } finally {
      clearTimeout(nightmareHalter);
    }
  }

  async cycleDownloadUntilSuccess(yearAndMonth: [number, number],
    tnvedDigits: TnvedOptions)
    : Promise<DownloadedFileInfo> {
    const t10mins = 1000 * 60 * 10;
    for (let i = 1; ; i += 1) {
      try {
        // eslint-disable-next-line no-await-in-loop
        const downloadedDbf = await this.downloadDbfFile(yearAndMonth, tnvedDigits, t10mins);
        console.log('File was downloaded:', downloadedDbf);
        return downloadedDbf;
      } catch (e) {
        console.log(`Error on ${i} attempt.`, e);
      }
    }
  }

  async end() {
    await this.nightmare.end();
  }
}
