/* eslint-disable @typescript-eslint/no-unused-vars */
import ClickHouse from '@apla/clickhouse';
import { CustomsRecord } from './CustomsRecord';
import { listen } from './server';
import extractFromDbf from './extractFromDbf';
import { CustomsStatisticsProvider, ENCODING } from './downloadingDbf';
import { ensureTableCreated, insert } from './postgresUtils';

require('dotenv').config();

const { TWO_CAPTCHA_KEY } = process.env;
const whiteList = new Set(process.env.ACCESS_CONTROL_ORIGINS?.replace(/ /g, '').split(','));
const downloadDir = 'downloads';

const client = new ClickHouse({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
});

async function parseStoreAndExit(paths: string[]) {
  /* eslint-disable no-await-in-loop */
  for (let i = 0; i < paths.length; i += 1) {
    const records = await extractFromDbf(paths[i], ENCODING);
    console.log(`From "${paths[i]}" extracted ${records.length} rows. Storing them into DB...`);
    await insert(client, records);
  }
  /* eslint-enable no-await-in-loop */
  process.exit(0);
}

(async () => {
  await ensureTableCreated(client);
  if (process.argv.length >= 4 && process.argv[2] === 'store') {
    const paths = process.argv.slice(3);
    await parseStoreAndExit(paths);
  }

  // const provider = new CustomsStatisticsProvider({downloadDir, TWO_CAPTCHA_KEY});
  // const dbfFile = await provider.cycleDownloadUntilSuccess([2017, 1], '2');
  // await provider.end();

  const httpPort = Number(process.env.SERVER_PORT);
  const httpsPort = Number(process.env.SSL_SERVER_PORT);
  listen(httpPort, httpsPort, whiteList);
})();
