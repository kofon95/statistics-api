import fs from 'fs';
import http from 'http';
import https from 'https';
import url from 'url';
import express from 'express';
import ClickHouse from '@apla/clickhouse';
import { encodeCountry, decodeCountry } from './extractFromDbf';

type QueryParams = {
  direction, dateFrom, dateTo, tnved, countries, regions,
};

// {..., country: 1} => {..., country: 'AB'}
function withDecodingCountry(record: {country: number}): {country: string} {
  return { ...record, country: decodeCountry(record.country) };
}

function sanitize(value: string): string {
  return `'${value.replace(/'/g, "''")}'`;
}
function sanitizeNumber(value: any): Number {
  return Number(value);
}
function sanitizeStartsWith(value: string): string {
  return `'${value.replace(/'/g, "''")}%'`;
}

// "201803" => "'2018-03-01'"
function sanitizeDate(date: string): string {
  if (date.includes("'")) throw new TypeError(`Wrong value of the date ${date}`);
  const year = date.substring(0, 4);
  const month = date.substring(4);
  const day = '01';
  return `'${year}-${month}-${day}'`;
}

function sanitizeIn(value: string): string {
  return value.split(',').map(sanitize).join(',');
}

function makeConditions(params: QueryParams): string[] {
  const conditions: string[] = [];
  if ('direction' in params) {
    conditions.push(`direction=${sanitizeNumber(params.direction)}`);
  }
  if ('dateFrom' in params) {
    conditions.push(`date>=${sanitizeDate(params.dateFrom)}`);
  }
  if ('dateTo' in params) {
    conditions.push(`date<=${sanitizeDate(params.dateTo)}`);
  }
  if (params.tnved) {
    conditions.push(`tnved LIKE ${sanitizeStartsWith(params.tnved)}`);
  }
  if (params.regions) {
    conditions.push(`region IN (${sanitizeIn(params.regions)})`);
  }
  if (params.countries) {
    const countries = params.countries
      .split(',')
      .map(encodeCountry)
      .map(sanitizeNumber)
      .join(',');
    conditions.push(`country IN (${countries})`);
  }
  return conditions;
}

function makeWhereClause(params: QueryParams): string {
  const conditions = makeConditions(params);
  if (conditions.length === 0) { return ''; }
  return `WHERE ${conditions.join(' AND ')}`;
}

const defaultAggregatesToSelect = 'sum(price) priceSum, sum(massInKG) massSum, count(*) count';

function makeSqlAggregation(whereClause: string, selectColumns: string, groupByColumns: string) {
  return `
    select ${defaultAggregatesToSelect}, ${selectColumns}
    from CustomsRecords
    ${whereClause}
    group by ${groupByColumns}`;
}

const privateKey = fs.readFileSync('sslcert/key.pem', 'utf8');
const certificate = fs.readFileSync('sslcert/cert.pem', 'utf8');

const credentials = { key: privateKey, cert: certificate };
const app = express();
const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

const clickhouse = new ClickHouse({
  host: 'localhost',
});

async function query(sql: string): Promise<any[]> {
  const { data, meta } = await clickhouse.querying(sql);
  return data.map((line: any[]) => line.reduce((acc, value, i) => {
    acc[meta[i].name] = value;
    return acc;
  }, {}));
}

// eslint-disable-next-line import/prefer-default-export
export async function listen(httpPort: number, httpsPort: number, whiteList: Set<string>) {
  // CORS on ExpressJS
  // https://enable-cors.org/server_expressjs.html
  app.use((req, res, next) => {
    const { origin } = req.headers;
    // TODO: this is commented in dev mode. Uncommend afterwards
    // if (whiteList.has(origin)) {
    res.header('Access-Control-Allow-Origin', origin);
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    // }
    next();
  });

  app.get('/', async (req, res) => {
    const params = url.parse(req.url, true).query as QueryParams;

    let summaryByDirectionAndMonth: any[];
    let summaryByCountry: any[];
    let summaryByRegion: any[];
    let summaryByTnved: any[];
    try {
      const tnvedGroupingBy = `substr(tnved, 1, ${(params.tnved || '').length + 2})`;
      const tnvedSelection = `${tnvedGroupingBy} tnvedPrefix`;
      const whereClause = makeWhereClause(params);

      [
        summaryByDirectionAndMonth,
        summaryByCountry,
        summaryByRegion,
        summaryByTnved,
      ] = await Promise.all([
        query(makeSqlAggregation(whereClause, 'date, direction', 'date, direction')),
        query(makeSqlAggregation(whereClause, 'country', 'country')),
        query(makeSqlAggregation(whereClause, 'region', 'region')),
        query(makeSqlAggregation(whereClause, tnvedSelection, tnvedGroupingBy)),
      ]);
    } catch (error) {
      console.error(error);
      res.header('Cache-Control', 'no-store');
      res.sendStatus(500);
      return;
    }

    res.setHeader('Content-Type', 'application/json');
    res.send({
      summaryByDirectionAndMonth,
      summaryByCountry: summaryByCountry.map(withDecodingCountry),
      summaryByRegion,
      summaryByTnved,
    });
  });
  httpServer.listen(httpPort, () => console.log(`Listenning: ${httpPort}`));
  httpsServer.listen(httpsPort, () => console.log(`Listenning SSL: ${httpsPort}`));
}
