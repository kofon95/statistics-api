/* eslint-disable camelcase */
import dbfstream from 'dbfstream';
import { CustomsRecord, DirectionType } from './CustomsRecord';

const charCodeOf_A = 'A'.charCodeAt(0);

// CD => 2*26 + 3 where 2 => 'C', 3 => 'D'
export function encodeCountry(country: string): number {
  const c1 = country.charCodeAt(0) - charCodeOf_A;
  const c2 = country.charCodeAt(1) - charCodeOf_A;
  return c1 * 26 + c2;
}

export function decodeCountry(n: number): string {
  if (!(n >= 0 && n < 26 * 26)) {
    throw new RangeError(`Out of range: ${n}`);
  }
  const c2 = n % 26;
  const c1 = (n - c2) / 26;
  return String.fromCharCode(c1 + charCodeOf_A) + String.fromCharCode(c2 + charCodeOf_A);
}

function encodeDirection(direction: string): DirectionType {
  switch (direction) {
    case 'ИМ':
      return 1;
    case 'ЭК':
      return 2;
    default:
      throw new TypeError(`Direction must be either "ИМ" or "ЭК", got ${direction}`);
  }
}

export function decodeDirection(direction: DirectionType): string {
  switch (direction) {
    case 1:
      return 'ИМ';
    case 2:
      return 'ЭК';
    default:
      throw new TypeError(`Direction must be either 1 or 2, got ${direction}`);
  }
}

function encodeRegion(region: string): number {
  return parseInt(region, 10);
}

declare class CustomsRecordOrigin {
  NAPR: string

  PERIOD: string

  STRANA: string

  TNVED: string

  EDIZM: string

  STOIM: number

  NETTO: number

  KOL: number

  REGION: string

  REGION_S: string
}

function convertToCustomRecord(record: CustomsRecordOrigin): CustomsRecord {
  return <CustomsRecord>{
    direction: encodeDirection(record.NAPR),
    date: record.PERIOD,
    country: encodeCountry(record.STRANA),
    tnved: record.TNVED,
    unit: record.EDIZM || null,
    price: record.STOIM,
    massInKG: record.NETTO,
    count: record.KOL,
    region: encodeRegion(record.REGION),
    // _origin: record, // enable for debugging purposes
  };
}

export default function extractFromDbf(path: string, encoding: string) {
  return new Promise<CustomsRecord[]>((res, rej) => {
    const dbf = dbfstream(path, encoding);

    const data: CustomsRecordOrigin[] = [];
    dbf.on('data', (record: CustomsRecordOrigin) => data.push(record));

    dbf.on('end', (err: Error) => {
      if (err) {
        rej(err);
        return;
      }
      data.pop(); // remove the last empty row
      // console.log('resolving a promise with: ', data.length);
      res(data.map(convertToCustomRecord));
    });
  });
}
