export type DirectionType = 1 | 2;

export declare class CustomsRecord {
  date: string

  direction: DirectionType

  country: number

  region: number

  tnved: string

  price: number

  massInKG: number

  count: number

  unit: string | null
}
