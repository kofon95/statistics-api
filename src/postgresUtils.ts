import ClickHouse from '@apla/clickhouse';
import { CustomsRecord } from './CustomsRecord';

// "03/2018" => "'2018-03-01'"
function sanitizeDate(date: string): string {
  if (date.length !== 7) {
    throw new Error(`Date should have the length of 7: ${date}`);
  }
  const month = date.substring(0, 2);
  const year = date.substring(3);
  return `'${year}-${month}-01'`;
}

function sanitizeParams(...args: any[]): (string)[] {
  return args.map((a) => {
    switch (typeof a) {
      case 'number':
        return a.toString();
      case 'string':
        return `'${a.replace(/'/g, "''")}'`;
      case 'boolean':
        return Number(a).toString();
      default:
        throw new TypeError(`Wrong type: "${typeof a}" of ${a}`);
    }
  });
}

function sanitizeCustomsRecord(r: CustomsRecord): string {
  const sanitized = [sanitizeDate(r.date)];
  sanitized.push(...sanitizeParams(
    r.direction, r.country, r.region, r.tnved,
    r.price, r.massInKG, r.count, r.unit || '',
  ));
  return sanitized.join(', ');
}

export async function ensureTableCreated(client: ClickHouse): Promise<any> {
  return client.querying(`
    create table if not exists CustomsRecords (
      date Date,
      direction Int8,
      country Int16,
      region Int32,
      tnved String,
      price Float64,
      massInKG Float64,
      count Float64,
      unit String
    ) ENGINE = MergeTree()
    ORDER BY date
  `);
}

export async function insert(client: ClickHouse, records: CustomsRecord[])
                            : Promise<any> {
  const values = records.map((d) => `(${sanitizeCustomsRecord(d)})`);
  const insertSql = `
    insert into CustomsRecords(
      date, direction, country, region,
      tnved, price, massInKG, count, unit)
    values ${values.join(', ')}`;
  // console.log(insertSql.slice(0, 200));
  return client.querying(insertSql);
}
