# Setup

```
npm install
```

Then create the `.env` file in the root of the project.

Copy the content of `.env.example` into that file and modify accordingly.

### SSL

Inside the `sslcert/` directory, generate certificates:
```
openssl genrsa -out key.pem
openssl req -new -key key.pem -out csr.pem
```
set them it and
```
openssl x509 -req -days 9999 -in csr.pem -signkey key.pem -out cert.pem
rm csr.pem
```
Source: https://nodejs.org/en/knowledge/HTTP/servers/how-to-create-a-HTTPS-server/

### WSL
In case you are running it in WSL, try
```
sudo apt-get install -y xvfb x11-xkb-utils xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic x11-apps clang libdbus-1-dev libgtk2.0-dev libnotify-dev libgnome-keyring-dev libgconf2-dev libasound2-dev libcap-dev libcups2-dev libxtst-dev libxss1 libnss3-dev gcc-multilib g++-multilib
```

Then prepend with `xvfb` your command, for example, `xvfb node dist/app.js`

### Development
In one terminal, run the TypesSript watch:
```
npm run watch
```

In another terminal, execute the compiled JS:
```
node dist/app.js
```

### Non-development
Simply run:
```
npm start
```

# Parse DBF and store data to DB
Run:
```
npm run parse_and_store PATH_TO_DBF
```
